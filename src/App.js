import React ,{useState}from "react";
import "./App.css";
import AppNavbar from "./components/AppNavBar";
import Login from "./pages/Login";
import Home from "./pages/Home";
import Course from "./pages/Course";
import Register from "./pages/Register";
import Logout from "./pages/Logout";
import Error from "./pages/Error"
import { Container } from "react-bootstrap";


import { UserProvider } from "./userContext";

// routes
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

function App() {

const [user, setUser] = useState({
  accessToken : localStorage.getItem('accessToken'),
  email : localStorage.getItem("email")
});

const unsetUser = () =>{
  localStorage.clear()
}


  return (
    
    <UserProvider value={{user,setUser,unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/courses" element={<Course />}/>
            <Route path="/register" element={<Register/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="*" element={<Error/>} />
  
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
