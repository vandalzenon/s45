const courseData = [
  {
    id: "wdc01",
    name: "PHP -Laravel",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita, ad facilis fugiat iusto sed unde cumque maxime blanditiis animi officiis fuga eveniet voluptatem hic dolore eius iste sunt rerum itaque.",
      price:40000
  },
  {
    id: "wdc02",
    name: "Phyton -Django",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita, ad facilis fugiat iusto sed unde cumque maxime blanditiis animi officiis fuga eveniet voluptatem hic dolore eius iste sunt rerum itaque.",
      price:35000
  },
  {
    id: "wdc03",
    name: "Java -Springbot",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita, ad facilis fugiat iusto sed unde cumque maxime blanditiis animi officiis fuga eveniet voluptatem hic dolore eius iste sunt rerum itaque.",
      price:2000
  }
];
export default courseData;