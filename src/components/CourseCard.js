import React from "react";
import { Card, Button } from "react-bootstrap";
import Proptypes from "prop-types";
import { useState, useEffect } from "react";

export default function CourseCard({ courseProp }) {
  const { name, price, description } = courseProp;
  let [count, setCount] = useState(30);
  const [isOpen, setIsOpen] = useState(true);
  const enroll = () => {
    setCount(count - 1);
    if (count < 1) {
      setCount((count = 0));
    }
  };

  useEffect(() => {
    if (count < 1) {
      setIsOpen(false);
    }
  }, [count]);

  return (
    <>
      <Card className="mt-2">
        <Card.Body>
          <Card.Title>{name}</Card.Title>

          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>Php {price}</Card.Text>
          <Card.Text>Available seats: {count}/30</Card.Text>
          {isOpen ? (
            <Button variant="primary" onClick={enroll}>
              Enroll
            </Button>
          ) : (
            <Button variant="secondary" onClick={enroll} disabled>
              Enroll
            </Button>
          )}
        </Card.Body>
      </Card>
    </>
  );
}

CourseCard.propTypes = {
  courseProp: Proptypes.shape({
    name: Proptypes.string.isRequired,
    description: Proptypes.string.isRequired,
    price: Proptypes.number.isRequired,
  }),
};
