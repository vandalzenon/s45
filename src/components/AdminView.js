import React, { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import AddCourse from './AddCourse';

export default function Adminview({coursesData}){

    const [courses ,setCourses] = useState([])
    useEffect(()=>{
       
        const coursesArr = coursesData.map(course =>{
            return(
                <tr key={course._id}>
                    <td>{course._id}</td>
                    <td>{course.name}</td>
                    <td>{course.description}</td>
                    <td>{course.price}</td>
                    <td className={course.isActive ? "text-success" : "text-danger"}>{course.isActive ? "Available" : "Unavailable"}</td>
                </tr>
            )
        })
        setCourses(coursesArr)
    })
    return(
        <>
        
        <div className="text-center-my-4"><h1>Admin Dashboard</h1></div>
        <AddCourse />
        <Table>
            <thead striped bordered hover responsive>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Availabilty</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {courses}
            </tbody>
        </Table>
        </>
    )
}