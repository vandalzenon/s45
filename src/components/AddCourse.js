import React, { useState } from "react";
import { Button, Modal, Form } from "react-bootstrap";
import Swal from 'sweetalert2'

export default function AddCourse() {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, SetPrice] = useState("");
  
  const [showAdd, setShowAdd] = useState(false);

  const openAdd = () => setShowAdd(true);
  const closeAdd = () => setShowAdd(false);

  const addCourse =(e) =>{
      e.preventDefault();
      fetch(' https://b165-shared-api.herokuapp.com/courses/create',{
          method: "POST",
          headers:{
              'Content-Type':'application/json',
              'Authorization':`Bearer ${localStorage.getItem('accessToken')}`
          },
          body:JSON.stringify({
              name:name,
              description:description,
              price:price
         })
      }).then(result => result.json()).then(data=>{
          console.log(data);
          if(data){
              Swal.fire({
                  title: 'Sucess',
                  icon:'success',
                  text: 'Course successfully added'

              })
              closeAdd();
          }else{
            Swal.fire({
                title: 'Something went Wrong',
                icon:'error',
                text: 'Something went wrong.Please try again'

            })
          }
      })
  }
  return (
    <>
      <Button variant="primary" onClick={openAdd}>Add New Course</Button>

      {
        <Modal show={showAdd} onHide={closeAdd}>
          <Form onSubmit={(e) => addCourse(e)}>
            <Modal.Header closeButton>
              <Modal.Title>Add Course</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form.Group>
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="text"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  required
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type="text"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                  required
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="number"
                  value={price}
                  onChange={(e) => SetPrice(e.target.value)}
                  required
                />
              </Form.Group>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={closeAdd}>
                Close
              </Button>
              <Button variant="success" type="submit">
                Submit
              </Button>
            </Modal.Footer>
          </Form>
        </Modal>
      }
    </>
  );
}
