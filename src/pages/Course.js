import React, { useContext, useState, useEffect } from "react";
import Userview from "../components/UserView";
import Adminview from "../components/AdminView";
import UserContext from "../userContext";

export default function Course() {
  const { user } = useContext(UserContext);
  const [allCourses, setAllCourses] = useState([]);

  const fetchData = () => {
    fetch(" https://b165-shared-api.herokuapp.com/courses/")
      .then((result) => result.json())
      .then((data) => {
          console.log(data);
          setAllCourses(data);
    });
      
  };
  useEffect(() => {
    fetchData();
  }, []);
  return (
    <>
      {user.isAdmin === true ? (
        <Adminview coursesData={allCourses} />
      ) : (
        <Userview coursesData={allCourses} />
      )}
    </>
  );
}
