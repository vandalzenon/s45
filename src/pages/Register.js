import React,{ useEffect, useState,useContext }  from "react";
import {Form ,Button} from "react-bootstrap";
import swal from "sweetalert2";
import { Navigate } from "react-router-dom";
import UserContext from "../userContext";



export default function Register(){
    const [email, setEmail]  = useState('');
    const [password, setPassword] = useState('');
    const [password2, setPassword2] = useState('');
    const [fName, setFname] = useState('');
    const [lName , setLName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [isActive, setIsActive] = useState(false);
    const {user} = useContext(UserContext);

    useEffect(()=>{
        if ((email !== '' && password !== '' && password2 !== '' && fName !== '' && lName !== '' && mobileNo !== '')&&(password2 === password)){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email,password,password2])

    function registerUser(e) {
        e.preventDefault();
        setEmail('');
        setPassword('');
        setPassword2('');
        setFname('');
        setLName('');
        setMobileNo('');

        fetch(' https://b165-shared-api.herokuapp.com/users/register', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
                firstName:fName,
                lastName:lName,
                mobileNo:mobileNo,
				email: email,
				password: password
			})
		}).then(result => result.json()).then(data=> {
            console.log(data)
            if (data === true){
                swal.fire({
                    title:"well Done ",
                    icon:"success",
                    text:"You have sucessfully registered"
                    
                });
               return(<Navigate to = '/login'/>) 
            }else{
                swal.fire({
                title:"failed ",
                icon:"error",
                text:`${data.message}`
            });}
            
            
        })


        
    }


    return(
        (user.accessToken !== null)?
        <Navigate to="/login"/>
        :
        <Form  onSubmit={(e) => registerUser(e)}>
            <h1>Register</h1>
        <Form.Group>
            <Form.Label>First Name</Form.Label>
            <Form.Control type ="text" placeholder="Enter First Name" value={fName} onChange={e =>setFname(e.target.value)}/>
        </Form.Group>
         <Form.Group>
            <Form.Label>Last Name</Form.Label>
            <Form.Control type ="text" placeholder="Enter Last Name" value={lName} onChange={e =>setLName(e.target.value)}/>
        </Form.Group>   
        <Form.Group>
            <Form.Label>Email Address</Form.Label>
            <Form.Control type ="email" placeholder="Enter Email" value={email} onChange={e =>setEmail(e.target.value)}/>
            <Form.Text className="text-muted">We'll never share your email with anyone else</Form.Text>
        </Form.Group>
        <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control type ="password" placeholder="Enter Password" value={password}  onChange={e =>setPassword(e.target.value)}/>
        </Form.Group>
        <Form.Group>
            <Form.Label>Verify Password</Form.Label>
            <Form.Control type ="password" placeholder="Verify Password" value={password2}  onChange={e =>setPassword2(e.target.value)}/>
        </Form.Group>
        <Form.Group>
            <Form.Label>Mobile Number</Form.Label>
            <Form.Control type ="text" placeholder="Enter Mobile Number" value={mobileNo} onChange={e =>setMobileNo(e.target.value)}/>
        </Form.Group> 
        {isActive ?
        <Button variant="primary" type="submit" className="mt-1" >Submit</Button>
        :
        <Button variant="secondary" type="submit" className="mt-1" disabled>Submit</Button>
        }
        
    </Form>
    )
}


